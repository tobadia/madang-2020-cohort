# Asia-Pacific ICEMR Madang Cohort (2020)

As part of the Asia-Pacific ICEMR, a cohort was followed-up in the Madang region in Papua New-Guinea. Due to the unforseen COVID-19 outbreak, enrolment was stopped and follow-up conducted only for a handful of participants, during the 1st month.
This repository will hold R scripts to harmonize data extraction and analysis.

Access to the database in these scripts will be handled through your API key. **It is important that you do not commit your API key, as it is private !**

## Prerequisites
This repository assumes that you have the following:
* R installed, along with the packages mentioned in the source files (`ggplot2`, `ggmap`, `cowplot`, `tidyverse` suite, `chron`, `redcapAPI`, `openxlsx`, `dotenv` )
* an API key to https://redcap.pasteur.fr/api/ valid for the Madang 2020 Cohort

### Usage
Ideally, open the `PNG 2020 Cohort.Rproj` file with RStudio and explore the varity of scripts and methods available.

### REDCap API key
Your API key is private. To streamline the access to the database, a dummy [.env.example file](https://gitlab.pasteur.fr/tobadia/madang-2020-cohort/-/blob/master/.env.example) is available in the repository. You should not modify it, but rather copy it, remove the `.example` extension and add your API key in the newly created file. The `.env` file is included in the `.gitignore` so that Git will never upload your `.env` file with your private API key.

```
cp .env.example .env
vi .env
```

This `.env` file will be sourced and imported to your R environmen each time you source the R files containing all the functions and resources developed for thsi project.

### R methods and resources
I have tried to compile all methods, package requirements etc. into a single file so that, when sourced, your R environment is fully loaded with everything that is needed for subsequent analyses. This file is [Madang_CSS_functions.R](https://gitlab.pasteur.fr/tobadia/madang-2020-cohort/-/blob/master/Madang_CSS_functions.R)

The **first** thing your should do when starting to work with this project is to run, within R :
```
source("./Madang_Cohort_functions.R")
```
Any error or warning raised here should be investigated, as there's a good chance it will impact subsequent analyses.


## What can I do ?

In the following, I will document what each script enables you to do. This README section should be updated when new features are developed... (keep fingers crossed)

### Database dump and create a working dataset
Due to technical constraints, the database was collected with multiple instruments, namely :

* Enrolment form (only used at first time point) : variables are prefixed with `en_`
* Follow-up form (repeated once per time point) : variables are prefixed with `fup_`
* Morbidity form (distinct infinitely-repeating time point) : variables are prefixed with `mob_`

In database, there is one row per participant and time point, inevitably causing "blocks" of missing values for columns not related to the time point currently recorded.

These forms share a somewhat common structure but the variables have to be gathered into a common column. For each occurence of a `measurement`, the content of [`en_measurement`, `fup_measurement` and `mob_measurement`] spread across 3 columns and 3+ rows should be carried over to a true "long-format" dataset such as [`time point`, `measurement`].

This conversion, along with some proper variable renaming and other curation, is done with help of the `madang_wide2long()` function. The [PNG_2020_Cohort_REDCap_database_export.R](https://gitlab.pasteur.fr/tobadia/madang-2020-cohort/-/blob/master/PNG_2020_Cohort_REDCap_database_export.R) file, when sourced, will dump the database (using your API key, sourced from the `.env` file), and create a master dataset named `madang.cohort` that is reshaped and curated. It's important we try and work as much as possible from this consolidated version.

**TL;DR**
R code
```
source("./PNG_2020_Cohort_REDCap_database_export.R")
head(madang.cohort)
```

Output
```
# A tibble: 6 x 209
# Groups:   subjid [3]
  subjid redcap_event_na… redcap_repeat_i… redcap_repeat_i… enrolment_form_… followup_form_c…
  <chr>  <chr>            <lgl>                       <int> <chr>            <chr>           
1 5001   enrollment_arm_1 NA                             NA Complete         NA              
2 5001   followup_bleed_… NA                             NA NA               Complete        
3 5002   enrollment_arm_1 NA                             NA Complete         NA              
4 5002   followup_bleed_… NA                             NA NA               Complete        
5 5003   enrollment_arm_1 NA                             NA Complete         NA              
6 5003   followup_bleed_… NA                             NA NA               Complete        
# … with 203 more variables: morbidity_form_complete <chr>, bl_hhid <dbl>,
#   bl_venblood_consent <chr>, bl_fname <chr>, bl_lname <chr>, bl_sex <chr>, bl_dob_known <chr>, …
```
